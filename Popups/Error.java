package Popups;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Error {
    public Error(String Title, String Message, Exception ex) {
        Frame f = new Frame(Title);
        Label l = new Label(Message);
        Button b = new Button("Ok");
        Button show = new Button("Show Error");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });
        show.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ex.printStackTrace();
            }
        });
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                f.dispose();
            }
        });
        f.add(l);
        f.add(b);
        f.add(show);
        f.setSize(250, 100);
        f.setLayout(new FlowLayout());
        f.setVisible(true);
    }
}
