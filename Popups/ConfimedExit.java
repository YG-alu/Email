package Popups;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ConfirmedExit {
    public void Confirmation() {
        Frame confirmedExit = new Frame("Confirmed Exit");
        Label Confirmation = new Label("Are you sure you want to exit?");
        Label nextLine = new Label("\n");
        Button yes = new Button("Yes");
        Button no = new Button("No");
        yes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        no.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmedExit.dispose();
            }
        });
        confirmedExit.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                confirmedExit.dispose();
            }
        });
        confirmedExit.add(Confirmation);
        confirmedExit.add(nextLine);
        confirmedExit.add(yes);
        confirmedExit.add(no);
        confirmedExit.setSize(250, 100);
        confirmedExit.setLayout(new FlowLayout());
        confirmedExit.setVisible(true);
    }
}