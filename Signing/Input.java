package Signing;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class Input {
    public static void main(String[] args) {
        Frame f = new Frame("Sign In");
        Button si = new Button("Sign In");
        Button su = new Button("Sign Up");
        signUp u = new signUp();
        Label text = new Label("Username:");
        Label nextLine = new Label("\n");
        TextField Username = new TextField(10);
        Label newlineText = new Label("\n Password:");
        TextField Password = new TextField(10);
        Button ok = new Button("Ok");
        /*
        Label newlineTextVerification = new Label("\n Verification:");
        TextField Verify = new TextField(10);

         */
        User U = new User();
        si.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signIn i = new signIn();
            }
        });
        su.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.add(text);
                f.add(Username);
                f.add(newlineText);
                f.add(Password);
                f.add(ok);
                /*
                f.add(newlineTextVerification);
                f.add(Verify);

                 */
                ok.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        U.setUsername(Username.getText());
                        U.setPassword(Password.getText());
                        System.out.println("Confirmation: \nYour Username: " + U.getUsername() + "\nYour Password: "
                                + U.getPassword());
                        try {
                            u.save();
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }
                });
            }
        });
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Frame confirmedExit = new Frame("Confirmed Exit");
                Label Confirmation = new Label("Are you sure you want to exit?");
                Button yes = new Button("Yes");
                Button no = new Button("No");
                yes.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });
                no.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        confirmedExit.dispose();
                    }
                });
                confirmedExit.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        confirmedExit.dispose();
                    }
                });
                confirmedExit.add(Confirmation);
                confirmedExit.add(nextLine);
                confirmedExit.add(yes);
                confirmedExit.add(no);
                confirmedExit.setSize(250, 100);
                confirmedExit.setLayout(new FlowLayout());
                confirmedExit.setVisible(true);
            }
        });
        f.add(si);
        f.add(su);
        f.setSize(200, 300);
        f.setLayout(new FlowLayout());
        f.setVisible(true);
    }
}
