package Sending;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

import Popups.Error;
import org.jetbrains.annotations.NotNull;

public class Main {
   public void send(String from, String password, String to, String sub, String msg) throws AuthenticationFailedException {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, password);

                    }
                });

    }

    public void MessageSent(String arg, @NotNull String arg2) {
        Frame f = new Frame("Compose");
        Label From = new Label("From: ");
        //TextField my_mail = new TextField(20);
        Label my_mail = new Label(arg);
        int length = arg2.length();
        Label password = new Label("Password: ");
        //TextField my_password = new TextField(20);
        Label my_password = new Label("*".repeat(length));
        Label To = new Label("\nTo: ");
        TextField totf = new TextField(20);
        Label subject = new Label("Subject:");
        TextField Subjecting = new TextField(20);
        TextArea ta = new TextArea("Message: ");
        Button send = new Button("Send");
        Main m = new Main();
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    m.send(From.getText(), my_password.getText(), totf.getText(), Subjecting.getText(), ta.getText());
                } catch (AuthenticationFailedException authenticationFailedException) {
                    authenticationFailedException.printStackTrace();
                }
            }
        });
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Frame confirmedExit = new Frame("Confirmed Exit");
                Label Confirmation = new Label("Are you sure you want to exit?");
                Label nextLine = new Label("\n");
                Button yes = new Button("Yes");
                Button no = new Button("No");
                yes.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });
                no.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        confirmedExit.dispose();
                    }
                });
                confirmedExit.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        confirmedExit.dispose();
                    }
                });
                confirmedExit.add(Confirmation);
                confirmedExit.add(nextLine);
                confirmedExit.add(yes);
                confirmedExit.add(no);
                confirmedExit.setSize(250, 100);
                confirmedExit.setLayout(new FlowLayout());
                confirmedExit.setVisible(true);
            }
        });
        f.add(From);
        f.add(my_mail);
        f.add(password);
        f.add(my_password);
        f.add(To);
        f.add(totf);
        f.add(subject);
        f.add(Subjecting);
        f.add(ta);
        f.add(send);
        f.setSize(500, 600);
        f.setLayout(new FlowLayout());
        f.setVisible(true);
    }

    public void Sending(String to, String from, String Subject, String Text) {
        String host = "localhost";

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);
            InternetAddress IntAdd = new InternetAddress();
            IntAdd.setAddress(from);
            message.setFrom(IntAdd.getAddress());
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(Subject);
            message.setText(Text);
            Transport.send(message);
        } catch (MessagingException mex) {
            Error er = new Error("Send Error", "      Couldn't send your message!       ", mex);
            /*
            Frame SendError = new Frame("Send Error");
            Label text = new Label("      Couldn't send your message!       ");
            Button Ok = new Button("Ok");
            Button SeeError = new Button("See error");
            SeeError.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    mex.printStackTrace();
                }
            });
            Ok.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SendError.dispose();
                }
            });
            SendError.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    SendError.dispose();
                }
            });
            SendError.add(text);
            SendError.add(Ok);
            SendError.add(SeeError);
            SendError.setSize(250, 100);
            SendError.setLayout(new FlowLayout());
            SendError.setVisible(true);

             */
        }
    }
}
/*
    public static void main(String[] args) {
        Frame f = new Frame("Compose");
        Label From = new Label("From: ");
        TextField my_mail = new TextField(20);
        Label To = new Label("\nTo: ");
        TextField totf = new TextField(20);
        Label subject = new Label("Subject:");
        TextField Subjecting = new TextField(20);
        TextArea ta = new TextArea("Message");
        Button send = new Button("Send");
        Main m = new Main();
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                m.Sending(totf.getText(), From.getText(), Subjecting.getText(), ta.getText());
            }
        });
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Frame confirmedExit = new Frame("Confirmed Exit");
                Label Confirmation = new Label("Are you sure you want to exit?");
                Label nextLine = new Label("\n");
                Button yes = new Button("Yes");
                Button no = new Button("No");
                yes.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });
                no.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        confirmedExit.dispose();
                    }
                });
                confirmedExit.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        confirmedExit.dispose();
                    }
                });
                confirmedExit.add(Confirmation);
                confirmedExit.add(nextLine);
                confirmedExit.add(yes);
                confirmedExit.add(no);
                confirmedExit.setSize(250, 100);
                confirmedExit.setLayout(new FlowLayout());
                confirmedExit.setVisible(true);
            }
        });
        f.add(From);
        f.add(my_mail);
        f.add(To);
        f.add(totf);
        f.add(subject);
        f.add(Subjecting);
        f.add(ta);
        f.add(send);
        f.setSize(500, 600);
        f.setLayout(new FlowLayout());
        f.setVisible(true);
    }
}

 */