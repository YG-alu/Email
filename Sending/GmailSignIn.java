package Sending;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import Popups.ConfirmedExit;

public class GmailSignIn {
    public static void main(String[] args) {
        Frame f = new Frame("Sign In");
        Label l = new Label("Gmail: ");
        TextField gmail = new TextField(10);
        Label nextLine = new Label("\n");
        Label l2 = new Label("Password: ");
        TextField password = new TextField(10);
        Button signin = new Button("Sign In");
        Main m = new Main();
        Popups.ConfirmedExit pc = new Popups.ConfirmedExit();
        signin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String x = gmail.getText();
                String y = password.getText();
                m.MessageSent(x, y);
            }
        });
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                pc.Confirmation();
            }
        });
        f.add(l);
        f.add(gmail);
        f.add(nextLine);
        f.add(l2);
        f.add(password);
        f.add(signin);
        f.setSize(200, 250);
        f.setLayout(new FlowLayout());
        f.setVisible(true);
    }
}
